## \file ex8.py
#  Este programa define o maior numero
#
#  É solicitado ao utilizador que insira tres valores
#  Apos isso verifica qual o maior numero atrazes de varias confições.


## Variável do primeiro numero
lado1 = float(input("Insira o numero: "))
## Variável do segundo numero
lado2 = float(input("Insira o numero: "))
## Variável do terceiro numero
lado3 = float(input("Insira o numero: "))
## Variável que define por defeito o maior como sendo o primeiro numero inserido
maior = lado1

if lado2 > maior:
    maior = lado2
if lado3 > maior:
    maior = lado3

print("o maior numero inserido é:", maior)
