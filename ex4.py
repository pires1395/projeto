## \file ex4.py
#  Este programa calcula o perimetro e a area de um circulo
#
#  Para realizar a operação é solicitado o raio do circulo.



## Variável a utilizar no ciclo for

import math

## Variável para guardar o raio
r = float(input("Insira o raio: "))

## Variável para calcular a area que é pi*(r*r)
area= math.pi*(r*r)
print(area)

## Variável para calcular o perimetro que é (2*pi)*r
perimetro=(2*math.pi)*r
print(perimetro)
