## \file ex7.py
#  Este programa gere as funçoes de uma maquina
#
#  Para que maquina realize as tarefas o utilizador
#  insere a letra para o que pretende fazer e vai aparecer uma menssagem no ecra como que o utilizador selecionou.


## Variável para ler a letra que o utilizador inserio
letra = str(input("Insira o que deseja fazer: "))
if letra == "l" or letra == "L" or letra == "D" or letra == "d" or letra == "f" or letra == "F":
    if letra == "l" or letra == "L":
        print("ligar")
    if letra == "D" or letra == "d":
        print("desligar")
    if letra == "f" or letra == "F":
        print("furar")
else:
    print("Comando não encontrado")
