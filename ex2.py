## \file ex2.py
#  Este programa apresenta o resultado da forma resolvente
#
#  Para realizar a formula resolvente são gerados 3 numeros
#  apos esses tres numeros serem gerados vai apresentar a soluçao.





from random import *

## Variável a utilizar na criação do primeiro numero
a=randint(0,10)


## Variável a utilizar na criação do segundo numero
b=randint(5,20)


## Variável a utilizar na criação do terceiro numero
c=randint(5,20)


## Variável a utilizar para calcular o (b*b)*4ac
d= b*b-4.0*a*c
if float(d)>0:
	
	## Variável a utilizar para calcular raiz quadrada de d
	rq=d**0.5
	
	## Variável a utilizar para calcular a soma de -b com com rq
	x1=(-b+rq)/2.0*a
	
	## Variável a utilizar para calcular a subtração de -b com com rq
	x2=(-b-rq)/2.0*a
	print(x1)
	print(x2)
else:
	print("Não existem raizes de numeros negativos")	
