## \file ex5.py
#  Este programa verifica se um aluno teve aprovação a uma disciplina
#
#  Para  verificar se o aluno foi aprovado o utilizador
#  insere a nota e o programa verifica atraves de condiçoes 
#  (if) verifica se foi aprovado ou nao.


## Variável que guarda a nota do aluno.
nota = float(input("Insira a nota: "))
if nota >=0 and nota <=20:
 if nota > 9.5:
    print("aprovado")
 else:
    print("reprovado")
else:
 print("insira uma nota valida")
