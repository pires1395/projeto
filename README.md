# Projeto de TPPI

**Bem Vindo**

No branche master é possivel encontrar todos os exercicios da aula de programaçao.
##Linguagem usada e como fazer o clone

Para a criação dos exercicicos foi utilizado o python3. 0 Python é uma linguagem de programação de alto nível,interpretada, de script, imperativa, orientada a objetos. A linguagem foi projetada com a filosofia de enfatizar a importância do esforço do programador sobre o esforço computacional. Prioriza a legibilidade do código.

Para obter todos os exercícios basta instalar na sua maquina o git e na linha de comandos inserir o comando seguinte:

$ git clone https://pires1395@bitbucket.org/pires1395/projeto.git

##Enunciado dos exercicios realizados
###Exercício 1

Implemente um programa que escreva como output “Olá mundo” 10 vezes.
###Exercício 2

Escreva o algoritmo e o programa para calcular a fórmula resolvente de equações de 2º grau . Os valores a, b, e c, devem ser gerados aleatoriamente. O valor de a varia entre [0, 10], e os valores b e c variam entre [5, 20]. Se (b^2 - 4ac) < 0, o output deve ser “não existem raízes reais”.
###Exercício 3

Implemente um programa para cambiar euros em dólares considerando a taxa de conversão 1,17.
###Exercício 4

Implemente um programa para determinar perímetro e área de circunferência.
###Exercício 5:

Implemente um programa para que verifique se o aluno é aprovado (se tem nota maior ou igual a 9,5)
###Exercício 6:

Implemente um programa para calcular a área e verifica se é um quadrado ou um retângulo
###Exercício 7:

Implemente um programa para modelar o funcionamento de uma máquina. Sendo que quando da inserção das letras: ’L’, ’D’ e ’F’, são apresentadas, respetivamente, as mensagens: Ligar, Desligar e Furar (use if...else)
###Exercício 8:

Implemente um programa para determinar o máximo de 3 valores
